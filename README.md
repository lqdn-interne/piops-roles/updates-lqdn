Updates-LQDN
=========

Un rôle qui se charge d'actier les mises à jours automatiques via unattend-upgrades.

Requirements
------------

None.

Role Variables
--------------

```
security_autoupdate_reboot: "false"
```

```
security_autoupdate_reboot_time: "03:00"
```

```
security_autoupdate_mail_to: "{{ sysadmin_email }}"
```

```
security_autoupdate_mail_on_error: true
```

```
security_autoupdate_blacklist : ""
```



Dependencies
------------

Aucun.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: updates-lqdn }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
